// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyDeathActor.generated.h"


UCLASS()
class SNAKEGAME_API AMyDeathActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyDeathActor();

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//������ ��������� ������� � ��������� �� ��������� ���������
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBoxComponent* MyRootComponent;

	//������ ������� (!) ��������. ����� ������� ������ ����� ���� ����� ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterialInstance* WallColor;

	//������ ����� �������� ������
	void KillVolume();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
