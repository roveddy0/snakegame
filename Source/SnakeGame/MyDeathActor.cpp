// Fill out your copyright notice in the Description page of Project Settings.


#include "MyDeathActor.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"


// Sets default values
AMyDeathActor::AMyDeathActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//������������� ���������, �������� �� ���
	class UStaticMesh* WallMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;
	WallColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("Script/Engine.MaterialInstanceConstant'/Game/M_Brick_Inst.M_Brick_Inst'")).Get();
	
	//������ ������� ������� ����������� 
	MyRootComponent = CreateDefaultSubobject<UBoxComponent>("RootComponent");
	RootComponent = MyRootComponent;

	///Script/Engine.MaterialInstanceConstant'/Game/M_Brick_Inst.M_Brick_Inst'
	
	//������������� �������� � �������� ����������
	class UStaticMeshComponent* WallElement;
	WallElement = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("KillCubeVolume"));
	WallElement->SetStaticMesh(WallMesh);
	WallElement->SetRelativeLocation(FVector(0, 0, 0));
	WallElement->SetMaterial(0, WallColor);
	WallElement->SetupAttachment(MyRootComponent);
	
	//����� �� ����������� ����
	WallElement->SetCastShadow(false);
}

// Called when the game starts or when spawned
void AMyDeathActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyDeathActor::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);
	KillVolume();

}

void AMyDeathActor::KillVolume()
{
	//������, ������� ������ � ������ �������� ��������� ������������
	TArray<AActor*> OverlappedActors;

	//� ��� ���������� �����?
	GetOverlappingActors(OverlappedActors);

	//���������� ���� ������� � ����� � �������� ��������� ������������ 
	for (int32 i = 0; i < OverlappedActors.Num(); ++i)
	{
		ASnakeElementBase* SnakeTouch = Cast<ASnakeElementBase>(OverlappedActors[i]);
		//������� �������, � �������� �����������
		OverlappedActors[i]->Destroy(true, true);
		
	}
}

