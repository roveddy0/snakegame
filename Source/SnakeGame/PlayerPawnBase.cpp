// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//�������� ��������� � ������ - ��� ������
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	//CreateSnakeActor();
	AddRandomApple();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//���� ������ ������� ������ ������� ��������, �� �������� �������� � � ���� ��������� �������� ������
	BufferTime += DeltaTime;
	if (BufferTime > DelayRespawn)
	{
		AddRandomApple();
		BufferTime = 0;
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)

	//���������� ������ ��� ����������
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

//������� ������ ������ ���� ��� ����������. � ���� ��� ����������, �� ������ ������� (������ ����) �� �������.
void APlayerPawnBase::CreateSnakeActor() 
{
	if (GetWorld())
	{
		SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
		//���� ���� ����� ���������� (�.�. ��������� ������ ����), �� �� ������������� ��� ������� ����� �� �������
		GameMode = 1;
	}

}

//��������� ����������� ������������ ���� � �����
void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor) && SnakeActor->Moving == false)
	{
		if (value > 0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor-> LastMoveDirection!=EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
			SnakeActor->Moving = true;
		}
	}

}

//��������� ����������� �������� ����� � ������
void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor) && SnakeActor->Moving == false)
	{
		if (value > 0 && SnakeActor->LastMoveDirection!=EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection!=EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
			SnakeActor->Moving = true;
		}
	}
}

void APlayerPawnBase::AddRandomApple()
{
	//����� �������
	FRotator StartPosition = FRotator(0, 0, 0);

	//���������� ��������� �����
	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxY);
	float SpawnZ = MaxScale;
	
	//���������� ����� ��� ���������� ���
	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	//���� ����� ����������, �� ������ ���
	if (SnakeActor)
	{
		GameMode = 1;

		//���� ����� (����) ����������, �� ������� ���
		if (GetWorld())
		{		
			//������� ������
			Food = GetWorld()->SpawnActor<AFood>(FoodActorClass, StartPoint, StartPosition);
		}
	}
}
//���� ��� SnakeActor ����������, �� �� ���������� ���������� �����, � ���� ������ �� ����������, �� ����� ���������� 0
int APlayerPawnBase::GetFoodScore()
{
	if (SnakeActor)
	{
		return SnakeActor->FoodScore;
	}
	return 0;
}

//���������� ��������� �����
/*/void APlayerPawnBase::ShowMouseCursor()
{
	APlayerController* ShowCursor = Cast<APlayerController>(GetController());
	if (ShowCursor)
	{
		ShowCursor->bShowMouseCursor = true;
		ShowCursor->bEnableClickEvents = true;
		ShowCursor->bEnableMouseOverEvents = true;
	}
}
*/
